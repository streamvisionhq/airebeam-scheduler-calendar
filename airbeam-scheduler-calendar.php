<?php
/*
  Plugin Name: Airbeam Scheduler Calendar
  Description: Creates an appointment scheduling calendar.
  Version:     1.3.1.4
  Author:      Codup.io
 */

if ( !defined('DS') ) {
    define("DS", DIRECTORY_SEPARATOR);
}


$package_ids = array(
      "B1_P0603" => 42,
      "B1_P0804" => 43,
      "B1_P1005" => 44,
      "B1_P1508" => 45,
      "B1_P2010" => 46,
      "B2_P0603" => 47,
      "B2_P0804" => 48,
      "B2_P1005" => 49,
      "B2_P1508" => 50,
      "B2_P2010" => 51,
      "B3_P0603" => 52,
      "B3_P0804" => 53,
      "B3_P1005" => 54,
      "B3_P1508" => 55,
      "B3_P2010" => 56,
      "B4_P0603" => 57,
      "B4_P0804" => 58,
      "B4_P1005" => 59,
      "B4_P1508" => 60,
      "B4_P2010" => 61
);

$package_service_mapping = array(
      42 => array("StreamVisionGold" => 121, "P0603" => 127),
      43 => array("StreamVisionGold" => 121, "P0804" => 129),
      44 => array("StreamVisionGold" => 121, "P1005" => 130),
      45 => array("StreamVisionGold" => 121, "P1608" => 132),
      46 => array("StreamVisionGold" => 121, "P2010" => 133),
      47 => array("StreamVisionGold" => 121, "DVR Basic" => 169, "P0603" => 127),
      48 => array("StreamVisionGold" => 121, "DVR Basic" => 169, "P0804" => 129),
      49 => array("StreamVisionGold" => 121, "DVR Basic" => 169, "P1005" => 130),
      50 => array("StreamVisionGold" => 121, "DVR Basic" => 169, "P1608" => 132),
      51 => array("StreamVisionGold" => 121, "DVR Basic" => 169, "P2010" => 133),
      52 => array("StreamVisionGold" => 121, "P0603" => 127, "ephone_local" => 33),
      53 => array("StreamVisionGold" => 121, "P0804" => 129, "ephone_local" => 33),
      54 => array("StreamVisionGold" => 121, "P1005" => 130, "ephone_local" => 33),
      55 => array("StreamVisionGold" => 121, "P1608" => 132, "ephone_local" => 33),
      56 => array("StreamVisionGold" => 121, "P2010" => 133, "ephone_local" => 33),
      57 => array("StreamVisionGold" => 121, "P0603" => 127, "ephone_local" => 33, "DVR Basic" => 169),
      58 => array("StreamVisionGold" => 121, "P0804" => 129, "ephone_local" => 33, "DVR Basic" => 169),
      59 => array("StreamVisionGold" => 121, "P1005" => 130, "ephone_local" => 33, "DVR Basic" => 169),
      60 => array("StreamVisionGold" => 121, "P1608" => 132, "ephone_local" => 33, "DVR Basic" => 169),
      61 => array("StreamVisionGold" => 121, "P2010" => 133, "ephone_local" => 33, "DVR Basic" => 169),
);

$sv_add_tv_id = 171;
$dvr_add_id = 168;
$dvr_storage_id = 160;
$ephone_canada_id = 35;
$ephone_intl_id = 32;

$phone_packages_mapping = array(
      //E-Phone Canada
      "1" => 35,
      //E-Phone International
      "2" => 32
);

$plan_mapping = array(
      "P0105" => 142,
      "P0201" => 125,
      "P0402" => 126,
      "P0603" => 127,
      "P0804" => 129,
      "P1005" => 130,
      "P1508" => 132,
      "P2010" => 133,
      "WC_WiFi" => 76
);

// Shortcode to output needed markup
add_shortcode('airbeam-scheduler-calendar', 'show_sheduler_calendar');

function show_sheduler_calendar() {
    if ( !isset($_GET['zip_code']) || empty($_GET['zip_code']) || !isset($_GET['mbr']) || empty($_GET['mbr']) || !$_COOKIE['tmpMbrAvl'] ) {
        wp_redirect(site_url() . '/order');
        exit;
    }

    $message = '<p style="margin-bottom: 0px;font-size: 33px;color: #1d3160; text-align: center;">Please wait... </p>';
    if ( !$_COOKIE['refreshCalendar'] ) {
        $message = '<p style="margin-top: 0px;font-size: 33px;color: #1d3160;text-align: center;">Please wait while the system is loading the available time slots in your area.</p>';
    }
    return '<div class="wp-loader-text" style="padding-top: 119px; padding-bottom: 205px; height: auto">' . $message . '</div><div id="root"></div>';
}

add_action('wp_enqueue_scripts', 'airbeam_scheduler_calendar_enqueue_scripts');

function airbeam_scheduler_calendar_enqueue_scripts() {

    global $post;
    if ( is_a($post, 'WP_Post') && has_shortcode($post->post_content, 'airbeam-scheduler-calendar') ) {
        wp_enqueue_script('airbeam-scheduler-script', plugin_dir_url(__FILE__) . 'build/static/js/main.85d4604f.js', array(), '0.0.1', true);
        wp_enqueue_style('airbeam-scheduler-style', plugin_dir_url(__FILE__) . 'build/static/css/main.9b99bc40.css');
    }

    if ( is_page('order') || is_page('plan-order-form') || is_page('order-2') ) {
        wp_enqueue_script('order-gform', plugin_dir_url(__FILE__) . 'build/assets/js/order-index.js', array(), '0.0.2', true);
    }
}

add_action('wp_footer', 'add_babel_script');

function add_babel_script() {
    global $post;
    if ( is_a($post, 'WP_Post') && has_shortcode($post->post_content, 'airbeam-scheduler-calendar') ) {
        ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.17.0/babel.min.js"></script>
        <?php
    }
}

// Add "babel" type to quiz script
add_filter('script_loader_tag', 'airbeam_scheduler_calendar_add_babel_type', 10, 3);

function airbeam_scheduler_calendar_add_babel_type($tag, $handle, $src) {
    if ( $handle !== 'airbeam-scheduler-script' ) {
        return $tag;
    }

    return '<script src="' . $src . '" type="text/babel"></script>' . "\n";
}

function zip_code_confirmation() {
    $error = false;
    if ( isset($_POST) && !empty($_POST) ) {

        if ( empty($_POST['zip_code']) ) {
            $error = true;
        }
        else {
            $zip_code = $_POST['zip_code'];
            $gfid = $_POST['gfid'];

            $entry = GFAPI::get_entry($gfid);
            $form = GFAPI::get_form($entry['form_id']);

            $entry['15'] = $zip_code;

            unset($entry['42.1']);
            unset($entry['42.4']);

            $result = GFAPI::update_entry($entry);
            GFAPI::send_notifications($form, $entry, 'form_submission');

            $url = "https://dispatch1.airebeam.net/api/check_zip_code?zip_code=" . $zip_code;

            $zip_response = wp_remote_get($url);

            if ( !is_wp_error($zip_response) && $_SESSION['orders_info'][$gfid] != '' ) {

                $form_data_array = $_SESSION['orders_info'][$gfid];
                unset($_SESSION['orders_info'][$gfid]);

                if ( $ticket_id = create_freshdesk_ticket($entry, $form) ) {
                    $form_data_array["ticket_id"] = $ticket_id;
                }

                $form_data_array['zip'] = $zip_code;

                $response = wp_remote_post('https://dispatch1.airebeam.net/api/create_mbr/post', array('body' => $form_data_array, 'timeout' => 15));

                if ( is_wp_error($response) ) {
                    $error_message = $response->get_error_message();
                    echo "Something went wrong: $error_message";
                    die();
                }
                else {
                    $body = wp_remote_retrieve_body($response);
                    $http_code = wp_remote_retrieve_response_code($response);

                    if ( $http_code == 200 && $body ) {
                        $resp_body = json_decode($body);


                        if ( json_decode($zip_response['body'])->message == 'Zip code does not found' ) {
                            wp_redirect(site_url() . '/no-service');
                            die();
                        }
                        else {
                            setcookie('tmpMbrAvl', 'true', 0, '/');
                            $url = site_url() . '/calendar/?mbr=' . $resp_body->mbr . '&zip_code=' . $form_data_array['zip'];
                            wp_redirect($url);
                            die();
                        }
                    }
                }
            }
        }
    }



    if ( isset($_GET['gfid']) && !empty($_GET['gfid']) ) {
        $gfid = $_GET['gfid'];

        include(plugin_dir_path(__FILE__) . DS . 'partials' . DS . 'zip-code-confirmation-screen.php');
    }
    else {
        wp_redirect(site_url());
        exit();
    }
}

add_shortcode('zip-code-confirmation', 'zip_code_confirmation');

function no_service() {

    include(plugin_dir_path(__FILE__) . DS . 'partials' . DS . 'no-service-area.php');
}

add_shortcode('no-service', 'no_service');

function prepare_form_data($gfid = false, $zipcode = null, $entry = false) {
    $service_ids = array();
    $package_service_ids = array();
    $plan_id = -1;

    if ( !$entry ) {
        $entry = GFAPI::get_entry($gfid);
    }

    if ( $zipcode != null ) {
        $entry[15] = $zipcode;
        $result = GFAPI::update_entry($entry);
    }
    global $package_ids;
    global $sv_add_tv_id;
    global $dvr_add_id;
    global $dvr_storage_id;
    global $ephone_canada_id;
    global $ephone_intl_id;
    global $phone_packages_mapping;
    global $plan_mapping;
    global $package_service_mapping;

    $form_data_array = array();
    $form_data_array['first_name'] = $entry[5];
    $form_data_array['last_name'] = $entry[6];
    $form_data_array['email'] = $entry[31];
    $form_data_array['address'] = $entry[13];
    $form_data_array['phone_home'] = $entry[8];
    $form_data_array['state'] = $entry[14];
    $form_data_array['zip'] = $entry[15];
    $form_data_array['city'] = $entry[1];
    $form_data_array['note'] = $entry[22];

    if ( !empty($entry[24]) ) {
        $form_data_array['package_id'] = $package_ids[$entry[24]];
        $package_array = $package_service_mapping[$package_ids[$entry[24]]];

        foreach ( $package_array as $key => $serv_val ) {
            // Push package services into package_service array. So find out easily what services does package has?
            array_push($package_service_ids, $serv_val);
        }
        $form_data_array['package_services'] = json_encode($package_service_ids);
    }
    array_push($service_ids, $plan_mapping[$entry[23]]);
    $plan_id = $plan_mapping[$entry[23]];



    for ( $count = 1; $count <= (int) $entry[34]; $count++ ) {
        array_push($service_ids, $sv_add_tv_id);
    }



    for ( $count = 1; $count <= (int) $entry[25]; $count++ ) {
        array_push($service_ids, $dvr_add_id);
    }



    for ( $count = 1; $count <= (int) $entry[26]; $count++ ) {
        array_push($service_ids, $dvr_storage_id);
    }



    array_push($service_ids, $phone_packages_mapping[$entry[27]]);

    if ( array_key_exists('package_id', $form_data_array) ) {
        // Remove service if user selects package. That service is already included in package.
        $search_key = array_search($plan_id, $service_ids);
        unset($service_ids[$search_key]);
    }

    $form_data_array['services'] = json_encode($service_ids);
    return $form_data_array;
}

function create_freshdesk_ticket($entry, $form) {
    // Creating Freshdesk ticket for this entry
    $notification = $form['notifications'][key($form['notifications'])];
    if ( rgget('subject', $notification) == "Online Service Order - Broadband" ) {

        $message = GFCommon::replace_variables(rgget('message', $notification), $form, $entry);

        $response = wp_remote_post('https://airebeam.freshdesk.com/api/v2/tickets/', array(
              'method' => 'POST',
              'headers' => array(
                    'Authorization' => 'Basic cFAxdEh6UkF0MjAxbTBCaTljMjo=',
                    'Content-type' => 'application/json'
              ),
              'body' => json_encode(array(
                    'subject' => 'Online Service Order - Broadband',
                    'email' => $entry['31'],
                    'type' => 'Online Order',
                    'status' => 2,
                    'priority' => 2,
                    'description' => $message,
                    'group_id' => 1000097319,
              )))
        );

        if ( !is_wp_error($response) ) {
            if ( json_decode($response['body'])->id ) {
                return json_decode($response['body'])->id;
            }
        }
        else {
            return false;
        }
    }
}

function populate_years($form) {
    foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos($field->cssClass, 'populate-years') === false ) {
            continue;
        }

        $start = date("Y");
        $end = date("Y") + 19;
        $years = range($start, $end);

        $choices = array();

        foreach ( $years as $year ) {
            $choices[] = array('text' => $year, 'value' => $year);
        }

        $field->choices = $choices;
    }

    return $form;
}

add_filter('gform_pre_render_1', 'populate_years');
add_filter('gform_pre_validation_1', 'populate_years');
add_filter('gform_pre_submission_filter_1', 'populate_years');
add_filter('gform_admin_pre_render_1', 'populate_years');

function filter_all_fields($value, $merge_tag, $modifier, $field) {

    if ( $merge_tag == 'all_fields' && ($field->id == '42' || $field->id == '42.1' || $field->id == '42.4') ) {
        return '';
    }
    else {
        return $value;
    }
}

add_filter('gform_merge_tag_filter', 'filter_all_fields', 10, 4);

require_once( 'includes/class-gf-capture-credit-card.php' );
