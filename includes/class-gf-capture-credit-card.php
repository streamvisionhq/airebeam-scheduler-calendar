<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ( !defined('DS') ) {
    define("DS", DIRECTORY_SEPARATOR);
}

add_action('gform_loaded', array('GF_Capture_Credit_Card_Bootstrap', 'load'), 5);

class GF_Capture_Credit_Card_Bootstrap {

    public static function load() {
        GFAddOn::register('GF_Capture_Credit_card');
    }

}

if ( method_exists('GFForms', 'include_payment_addon_framework') ) {
    GFForms::include_payment_addon_framework();

    class GF_Capture_Credit_card extends GFPaymentAddOn {

        protected $_version = "1.1.1.0";
        protected $_min_gravityforms_version = "1.9.14";
        protected $_slug = 'airbeam-scheduler-calendar';
        protected $_path = 'airbeam-scheduler-calendar/airbeam-scheduler-calendar.php';
        protected $_full_path = __FILE__;
        protected $_title = 'Gravity Forms Capture Credit Card';
        protected $_short_title = 'Capture CC';
        protected $_supports_callbacks = false;
        protected $_requires_credit_card = true;
        private static $_instance = null;

        public static function get_instance() {
            if ( self::$_instance == null ) {
                self::$_instance = new GF_Capture_Credit_card();
            }

            return self::$_instance;
        }

        public function option_choices() {
            return false;
        }

        public function feed_settings_fields() {
            return array(
                  array(
                        'description' => '',
                        'fields' => array(
                              array(
                                    'name' => 'feedName',
                                    'label' => esc_html__('Name', 'gravityforms'),
                                    'type' => 'text',
                                    'class' => 'medium',
                                    'required' => true,
                                    'tooltip' => '<h6>' . esc_html__('Name', 'gravityforms') . '</h6>' . esc_html__('Enter a feed name to uniquely identify this setup.', 'gravityforms')
                              )
                        )
                  )
            );
        }

        public function get_submission_data($feed, $form, $entry) {

            $entry_id = GFAPI::add_entry($entry);
            $entry = GFAPI::get_entry($entry_id);

            foreach ( $form['fields'] as $field ) {
                $value = rgar($entry, (string) $field->id);
                if ( (string) $field->id == "15" ) {
                    $zip = $value;
                }
            }

            $url = "https://dispatch1.airebeam.net/api/check_zip_code?zip_code=" . $zip;
            $response = wp_remote_get($url);

            $form_data_array = prepare_form_data(false, null, $entry);

            // Getting credit card field data.
            $card_field = $this->get_credit_card_field($form);
            if ( $card_field ) {
                $card_expiration_date = rgpost("input_{$card_field->id}_2");
                $card_expiration_date = date("F Y", strtotime("01-" . $card_expiration_date[0] . "-" . $card_expiration_date[1]));
                $form_data_array['cc_number'] = $this->remove_spaces_from_card_number(rgpost("input_{$card_field->id}_1"));
                $form_data_array['cc_expiration_date'] = $card_expiration_date;
                $form_data_array['cc_security_code'] = rgpost("input_{$card_field->id}_3");
                $form_data_array['cc_name'] = rgpost("input_{$card_field->id}_5");
            }

            unset($entry['42.1']);
            unset($entry['42.4']);

            if ( !session_id() ) {
                session_start();
            }
            $_SESSION['orders_info'][$entry['id']] = $form_data_array;
            
            if ( !is_wp_error($response) ) {
                if ( json_decode($response['body'])->message == 'Zip code does not found' ) {
                    wp_redirect(site_url() . '/confirm-your-zip-code?gfid=' . $entry['id']);
                    exit;
                }
            }
            
            GFAPI::send_notifications($form, $entry, 'form_submission');

            if ( $ticket_id = create_freshdesk_ticket($entry, $form) ) {
                $form_data_array["ticket_id"] = $ticket_id;
            }

            $response = wp_remote_post('https://dispatch1.airebeam.net/api/create_mbr/post', array('body' => $form_data_array, 'timeout' => 15));
            if ( is_wp_error($response) ) {
                $error_message = $response->get_error_message();
                echo "Something went wrong: $error_message";
            }
            else {
                $body = wp_remote_retrieve_body($response);
                $http_code = wp_remote_retrieve_response_code($response);
                if ( $http_code == 200 && $body ) {
                    $resp_body = json_decode($body);
                    setcookie('tmpMbrAvl', 'true', 0, '/');
                    $url = site_url() . '/calendar/?mbr=' . $resp_body->mbr . '&zip_code=' . $form_data_array['zip'];
                    wp_redirect($url);
                    exit;
                }
            }
        }

    }

}
