
import React from 'react';
import ReactDOM from 'react-dom';
import BigCalendar from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import './style.css';
import Loader from './ajax-loader.gif';

import Moment from 'moment';
import cookie from 'react-cookies'
        import { extendMoment } from 'moment-range';

const moment = extendMoment(Moment);
BigCalendar.momentLocalizer(moment);



class MyCalendar extends React.Component {

    constructor(props) {
        super(props);
        this.createAppointment = this.createAppointment.bind(this);
        this.confirmAppointment = this.confirmAppointment.bind(this);
        this.cancelAppointment = this.cancelAppointment.bind(this);
        this.state = {
            view: 'week',
            slots: [],
            startOfWeek: '',
            startTime: '',
            endTime: '',
            worker_slots: [],
            is_slot_selected: false,
            slotSelectedStart: '',
            slotSelectedEnd: '',
            appointmentCreated: false,
            refreshCalendar: cookie.load('refreshCalendar') || false,
            zipCodeNotFound: false,
            confirmAppointment: false,
            appointmentInProcess: false

        }

    }

    componentDidMount() {

        let zip = this.urlParam("zip_code");

        fetch('https://dispatch1.airebeam.net/api/available_slots/get?zip_code=' + zip)
                .then(res => res.json())
                .then((jsonResult) => {
                    let slotsArray = toArray(jsonResult);
                    let timeRange = getTimeRange(slotsArray);
                    let start_time = new Date(moment(timeRange.startTime)).setSeconds(0);
                    let end_time = timeRange.endTime;
                    if (new Date(timeRange.endTime).getMinutes() < 30) {
                        end_time = new Date(moment(end_time)).setMinutes(0);
                        end_time = new Date(moment(end_time)).setSeconds(0);
                    } else {

                        end_time = new Date(moment(end_time)).setMinutes(30);
                        end_time = new Date(moment(end_time)).setSeconds(0);

                    }
                    this.setState({
                        slots: getSlots(slotsArray),
                        startOfWeek: slotsArray[0].date,
                        startTime: start_time,
                        endTime: end_time,
                        workerSlots: getWorkerSlots(slotsArray),
                        workerId: '',
                        zipCodeNotFound: false
                    });
                    console.log(this.state.startOfWeek);
                })
                .catch(err => {

                });

    }

    urlParam(name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
            return null;
        } else {
            return decodeURI(results[1]) || 0;
        }
    }

    makeAppoitmentSlot(workerSlots, slotObject, events) {

        if (this.state.confirmAppointment) {
            return;
        }

        let date = (slotObject.start.getDate() < 10 ? '0' : '') + slotObject.start.getDate();
        let month = ((slotObject.start.getMonth() + 1) < 10 ? '0' : '') + (slotObject.start.getMonth() + 1);
        let year = slotObject.start.getFullYear();
        let fullDate = year + '-' + month + '-' + date;
        let slotsForTheDay = workerSlots[fullDate].workerSlots;
        let minCommutetime = workerSlots[fullDate].minCommutetime;
        let installationTime = workerSlots[fullDate].installationTime;
        let startDayTime = moment.utc(new Date(workerSlots[fullDate].startTime.date));
        let endDayTime = moment.utc(new Date(workerSlots[fullDate].endTime.date));
        let maxAppointmentPerDay = workerSlots[fullDate].maxAppointmentPerDay;
        let slotAvailable = false;
        if (this.state.is_slot_selected) {

            events.splice(-1, 1);
            events.splice(-1, 1);


        }
        this.setState({
            is_slot_selected: false,
            refreshCalendar: false
        })

        //sort slots according to priority - ascending
        let slotsSortByPriority = [];
        for (let i = 0; i < slotsForTheDay.length; i++) {
            slotsSortByPriority[slotsForTheDay[i].worker_priority - 1] = slotsForTheDay[i];
        }
        slotsSortByPriority = slotsSortByPriority.filter(function (e) {
            return e
        });

        for (let i = 0; i < slotsSortByPriority.length; i++) {

            var slotStart = moment.utc(slotObject.start);
            var slotEnd = moment.utc(slotObject.end);

            //check if worker is free all day
            if (slotsSortByPriority[i]['worker_booked_slots'].length == 0) {

                slotEnd = moment.utc(moment(slotStart).add(installationTime + minCommutetime, 'm').toDate());

                if (moment(slotStart).isAfter(startDayTime)) {
                    slotStart = moment.utc(moment(slotStart).subtract(minCommutetime, 'm').toDate());
                    slotEnd = moment.utc(moment(slotEnd).subtract(minCommutetime, 'm').toDate());
                }

                if ((moment(slotEnd).isAfter(moment(endDayTime)))) {

                    for (let k = 0; k < ((minCommutetime + installationTime) / 30) - 1; k++) {

                        slotStart = moment.utc(moment(slotStart).subtract(30, 'm').toDate());
                        slotEnd = moment.utc(moment(slotEnd).subtract(30, 'm').toDate());

                        if (!(moment(slotEnd).isAfter(moment(endDayTime)))) {
                            break;
                        }
                    }
                }

                let selectedSlotCommuteTimeStart = new Date(moment(slotStart));
                let selectedSlotCommuteTimeEnd = new Date(moment(selectedSlotCommuteTimeStart).add(minCommutetime, 'm').toDate());
                let selectedStartTime = new Date(moment(selectedSlotCommuteTimeEnd));
                let selectedEndTime = new Date(moment(selectedStartTime).add(installationTime, 'm').toDate());


                let date = selectedStartTime.getDate();
                let month = ((selectedStartTime.getMonth() + 1) < 10 ? '0' : '') + (selectedStartTime.getMonth() + 1);
                let year = selectedStartTime.getFullYear();

                let startHours = selectedStartTime.getHours();
                let startMinutes = ((selectedStartTime.getMinutes()) < 10 ? '0' : '') + (selectedStartTime.getMinutes());
                let startSeconds = ((selectedStartTime.getSeconds()) < 10 ? '0' : '') + (selectedStartTime.getSeconds());

                let endHours = selectedEndTime.getHours();
                let endMinutes = ((selectedEndTime.getMinutes()) < 10 ? '0' : '') + (selectedEndTime.getMinutes());
                let endSeconds = ((selectedEndTime.getSeconds()) < 10 ? '0' : '') + (selectedEndTime.getSeconds());


                let fullDateStart = year + '-' + month + '-' + date + ' ' + startHours + ':' + startMinutes + ':' + startSeconds;
                let fullDateEnd = year + '-' + month + '-' + date + ' ' + endHours + ':' + endMinutes + ':' + endSeconds;

                events.push({
                    allDay: false,
                    title: 'Commute Time',
                    category: 'selected-slot-commute',
                    start: selectedSlotCommuteTimeStart,
                    end: selectedSlotCommuteTimeEnd,

                })

                events.push({
                    allDay: false,
                    title: 'Installation Time',
                    category: 'selected-slot-installtion',
                    start: selectedStartTime,
                    end: selectedEndTime,

                })

                this.setState({
                    slots: events,
                    is_slot_selected: true,
                    slotSelectedStart: fullDateStart,
                    slotSelectedEnd: fullDateEnd,
                    workerId: slotsSortByPriority[i]['worker_id'],
                    workerName: slotsSortByPriority[i]['worker_name']
                });

                slotAvailable = true;
                break;
            } else {
                //find worker free slot

                let goToNextWorker = false;

                //check if max day limit is exceeding for worker, go to next worker. 
                if (slotsSortByPriority[i]['worker_booked_slots'].length == maxAppointmentPerDay) {
                    goToNextWorker = true;
                    continue;
                }
                for (let j = 0; j < slotsSortByPriority[i]['worker_booked_slots'].length; j++) {

                    let workerStartTime = moment.utc(new Date(slotsSortByPriority[i]['worker_booked_slots'][j].start_time.date));
                    let workerEndTime = moment.utc(new Date(slotsSortByPriority[i]['worker_booked_slots'][j].end_time.date));
                    slotStart = moment.utc(slotObject.start);
                    slotEnd = moment.utc(slotObject.end);

                    let workerTimeRange = moment.range(workerStartTime, workerEndTime);
                    let slotRange = moment.range(slotStart, slotEnd);
                    if (!slotRange.overlaps(workerTimeRange)) {

                        let workerNextStartTime = moment();
                        let workerNextEndTime = moment();
                        let workerNextTimeRange = moment.range(workerNextStartTime, workerNextEndTime);
                        slotEnd = moment.utc(moment(slotStart).add(installationTime + minCommutetime, 'm').toDate());
                        slotRange = moment.range(slotStart, slotEnd);
                        workerStartTime = moment.utc(moment(workerStartTime).subtract(minCommutetime, 'm').toDate());
                        workerTimeRange = moment.range(workerStartTime, workerEndTime);

                        var commuteTimeBeforEndDay = moment.utc(moment(endDayTime).subtract(minCommutetime + installationTime, 'm').toDate());
                        if (moment(slotStart).isBefore(workerStartTime)) {
                            commuteTimeBeforEndDay = moment.utc(moment(workerStartTime).subtract(minCommutetime + installationTime, 'm').toDate());
                        }
                        if (j !== slotsSortByPriority[i]['worker_booked_slots'].length - 1) {
                            workerNextStartTime = moment.utc(new Date(slotsSortByPriority[i]['worker_booked_slots'][j + 1].start_time.date));
                            workerNextEndTime = moment.utc(new Date(slotsSortByPriority[i]['worker_booked_slots'][j + 1].end_time.date));
                            workerNextStartTime = moment.utc(moment(workerNextStartTime).subtract(minCommutetime, 'm').toDate());
                            workerNextTimeRange = moment.range(workerNextStartTime, workerNextEndTime);
                            commuteTimeBeforEndDay = moment.utc(moment(workerNextStartTime).subtract(minCommutetime + installationTime, 'm').toDate());

                        }

                        if (slotRange.overlaps(workerTimeRange) && moment(slotStart).isSame(moment(startDayTime))) {

                            goToNextWorker = true;
                            break;
                        }

                        if (!slotRange.overlaps(workerTimeRange)
                                && ((slotsSortByPriority[i]['worker_booked_slots'].length == 1)
                                        || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && moment(slotEnd).isSameOrBefore(workerStartTime))
                                        || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j !== slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStart).isSameOrAfter(workerEndTime) && moment(slotStart).isSameOrBefore(workerNextStartTime) && moment(slotEnd).isSameOrBefore(workerNextStartTime))
                                        || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j === slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStart).isSameOrAfter(workerEndTime) && moment(slotEnd).isSameOrAfter(workerEndTime))
                                        || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j === slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStart).isSameOrBefore(workerStartTime) && moment(slotEnd).isSameOrBefore(workerStartTime)))
                                && moment(slotStart).isSameOrAfter(moment(startDayTime))
                                && moment(slotEnd).isSameOrBefore(moment(endDayTime))
                                ) {
                            //start slot from possible start Time

                            if (moment(slotStart).isSameOrBefore(commuteTimeBeforEndDay)) {
                                let slotStartTwo = moment.utc(moment(slotStart).subtract(minCommutetime, 'm').toDate());
                                let slotEndTwo = moment.utc(moment(slotEnd).subtract(minCommutetime, 'm').toDate());
                                let slotRangeTwo = moment.range(slotStartTwo, slotEndTwo);
                                if (!slotRangeTwo.overlaps(workerTimeRange)
                                        && ((slotsSortByPriority[i]['worker_booked_slots'].length == 1)
                                                || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && moment(slotEndTwo).isSameOrBefore(workerStartTime))
                                                || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j !== slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStartTwo).isSameOrAfter(workerEndTime) && moment(slotStartTwo).isSameOrBefore(workerNextStartTime) && moment(slotEndTwo).isSameOrBefore(workerNextStartTime))
                                                || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j === slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStartTwo).isSameOrAfter(workerEndTime) && moment(slotEndTwo).isSameOrAfter(workerEndTime))
                                                || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j === slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStartTwo).isSameOrBefore(workerStartTime) && moment(slotEndTwo).isSameOrBefore(workerStartTime)))
                                        && moment(slotStartTwo).isSameOrAfter(moment(startDayTime))
                                        && moment(slotEndTwo).isSameOrBefore(moment(endDayTime))
                                        ) {
                                    slotStart = slotStartTwo;
                                    slotEnd = slotEndTwo;
                                } else {
                                    for (let k = 0; k < ((minCommutetime) / 30) - 1; k++) {

                                        slotStartTwo = moment.utc(moment(slotStartTwo).add(30, 'm').toDate());
                                        slotEndTwo = moment.utc(moment(slotEndTwo).add(30, 'm').toDate());
                                        slotRangeTwo = moment.range(slotStartTwo, slotEndTwo);

                                        if (!slotRangeTwo.overlaps(workerTimeRange)
                                                && ((slotsSortByPriority[i]['worker_booked_slots'].length == 1)
                                                        || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && moment(slotEndTwo).isSameOrBefore(workerStartTime))
                                                        || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j !== slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStartTwo).isSameOrAfter(workerEndTime) && moment(slotStartTwo).isSameOrBefore(workerNextStartTime) && moment(slotEndTwo).isSameOrBefore(workerNextStartTime))
                                                        || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j === slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStartTwo).isSameOrAfter(workerEndTime) && moment(slotEndTwo).isSameOrAfter(workerEndTime))
                                                        || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j === slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStartTwo).isSameOrBefore(workerStartTime) && moment(slotEndTwo).isSameOrBefore(workerStartTime)))
                                                && moment(slotStartTwo).isSameOrAfter(moment(startDayTime))
                                                && moment(slotEndTwo).isSameOrBefore(moment(endDayTime))
                                                ) {
                                            slotStart = slotStartTwo;
                                            slotEnd = slotEndTwo;
                                            break;
                                        }

                                    }
                                }

                            }

                            //end: start slot from possible start Time

                            slotAvailable = true;
                            let selectedSlotCommuteTimeStart = new Date(moment(slotStart));
                            let selectedSlotCommuteTimeEnd = new Date(moment(selectedSlotCommuteTimeStart).add(minCommutetime, 'm').toDate());
                            let selectedStartTime = new Date(moment(selectedSlotCommuteTimeEnd));
                            let selectedEndTime = new Date(slotEnd);


                            let date = selectedStartTime.getDate();
                            let month = ((selectedStartTime.getMonth() + 1) < 10 ? '0' : '') + (selectedStartTime.getMonth() + 1);
                            let year = selectedStartTime.getFullYear();

                            let startHours = selectedStartTime.getHours();
                            let startMinutes = ((selectedStartTime.getMinutes()) < 10 ? '0' : '') + (selectedStartTime.getMinutes());
                            let startSeconds = ((selectedStartTime.getSeconds()) < 10 ? '0' : '') + (selectedStartTime.getSeconds());

                            let endHours = selectedEndTime.getHours();
                            let endMinutes = ((selectedEndTime.getMinutes()) < 10 ? '0' : '') + (selectedEndTime.getMinutes());
                            let endSeconds = ((selectedEndTime.getSeconds()) < 10 ? '0' : '') + (selectedEndTime.getSeconds());

                            let fullDateStart = year + '-' + month + '-' + date + ' ' + startHours + ':' + startMinutes + ':' + startSeconds;
                            let fullDateEnd = year + '-' + month + '-' + date + ' ' + endHours + ':' + endMinutes + ':' + endSeconds;

                            events.push({
                                allDay: false,
                                title: 'Commute Time',
                                category: 'selected-slot-commute',
                                start: selectedSlotCommuteTimeStart,
                                end: selectedSlotCommuteTimeEnd,

                            })

                            events.push({
                                allDay: false,
                                title: 'Installation Time',
                                category: 'selected-slot-installtion',
                                start: new Date(selectedStartTime),
                                end: new Date(selectedEndTime),

                            })

                            this.setState({
                                slots: events,
                                is_slot_selected: true,
                                slotSelectedStart: fullDateStart,
                                slotSelectedEnd: fullDateEnd,
                                workerId: slotsSortByPriority[i]['worker_id'],
                                workerName: slotsSortByPriority[i]['worker_name']
                            });
                            break;


                        } else {

                            for (let k = 0; k < ((minCommutetime + installationTime) / 30) - 1; k++) {

                                slotStart = moment.utc(moment(slotStart).subtract(30, 'm').toDate());
                                slotEnd = moment.utc(moment(slotEnd).subtract(30, 'm').toDate());
                                slotRange = moment.range(slotStart, slotEnd);
                                if (slotRange.overlaps(workerTimeRange) && moment(slotStart).isSame(moment(startDayTime))) {

                                    goToNextWorker = true;
                                    break;
                                }
                                if (!slotRange.overlaps(workerTimeRange)
                                        && ((slotsSortByPriority[i]['worker_booked_slots'].length == 1)
                                                || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && moment(slotEnd).isSameOrBefore(workerStartTime))
                                                || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j !== slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStart).isSameOrAfter(workerEndTime) && moment(slotStart).isSameOrBefore(workerNextStartTime) && moment(slotEnd).isSameOrBefore(workerNextStartTime))
                                                || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j === slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStart).isSameOrAfter(workerEndTime) && moment(slotEnd).isSameOrAfter(workerEndTime))
                                                || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j === slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStart).isSameOrBefore(workerStartTime) && moment(slotEnd).isSameOrBefore(workerStartTime)))
                                        && moment(slotStart).isSameOrAfter(moment(startDayTime))
                                        && moment(slotEnd).isSameOrBefore(moment(endDayTime))
                                        ) {

                                    //start slot from possible start Time
                                    if (moment(slotStart).isBefore(commuteTimeBeforEndDay)) {
                                        let slotStartTwo = moment.utc(moment(slotStart).subtract(minCommutetime, 'm').toDate());
                                        let slotEndTwo = moment.utc(moment(slotEnd).subtract(minCommutetime, 'm').toDate());
                                        let slotRangeTwo = moment.range(slotStartTwo, slotEndTwo);

                                        if (!slotRangeTwo.overlaps(workerTimeRange)
                                                && ((slotsSortByPriority[i]['worker_booked_slots'].length == 1)
                                                        || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && moment(slotEndTwo).isSameOrBefore(workerStartTime))
                                                        || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j !== slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStartTwo).isSameOrAfter(workerEndTime) && moment(slotStartTwo).isSameOrBefore(workerNextStartTime) && moment(slotEndTwo).isSameOrBefore(workerNextStartTime))
                                                        || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j === slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStartTwo).isSameOrAfter(workerEndTime) && moment(slotEndTwo).isSameOrAfter(workerEndTime))
                                                        || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j === slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStartTwo).isSameOrBefore(workerStartTime) && moment(slotEndTwo).isSameOrBefore(workerStartTime)))
                                                && moment(slotStartTwo).isSameOrAfter(moment(startDayTime))
                                                && moment(slotEndTwo).isSameOrBefore(moment(endDayTime))
                                                ) {

                                            slotStart = slotStartTwo;
                                            slotEnd = slotEndTwo;
                                        } else {
                                            for (let k = 0; k < ((minCommutetime) / 30) - 1; k++) {

                                                slotStartTwo = moment.utc(moment(slotStartTwo).add(30, 'm').toDate());
                                                slotEndTwo = moment.utc(moment(slotEndTwo).add(30, 'm').toDate());
                                                slotRangeTwo = moment.range(slotStartTwo, slotEndTwo);

                                                if (!slotRangeTwo.overlaps(workerTimeRange)
                                                        && ((slotsSortByPriority[i]['worker_booked_slots'].length == 1)
                                                                || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && moment(slotEndTwo).isSameOrBefore(workerStartTime))
                                                                || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j !== slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStartTwo).isSameOrAfter(workerEndTime) && moment(slotStartTwo).isSameOrBefore(workerNextStartTime) && moment(slotEndTwo).isSameOrBefore(workerNextStartTime))
                                                                || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j === slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStartTwo).isSameOrAfter(workerEndTime) && moment(slotEndTwo).isSameOrAfter(workerEndTime))
                                                                || (slotsSortByPriority[i]['worker_booked_slots'].length !== 1 && j === slotsSortByPriority[i]['worker_booked_slots'].length - 1 && moment(slotStartTwo).isSameOrBefore(workerStartTime) && moment(slotEndTwo).isSameOrBefore(workerStartTime)))
                                                        && moment(slotStartTwo).isSameOrAfter(moment(startDayTime))
                                                        && moment(slotEndTwo).isSameOrBefore(moment(endDayTime))
                                                        ) {
                                                    slotStart = slotStartTwo;
                                                    slotEnd = slotEndTwo;
                                                    break;
                                                }

                                            }
                                        }
                                    }
                                    //end: start slot from possible start Time

                                    slotAvailable = true;


                                    let selectedSlotCommuteTimeStart = new Date(moment(slotStart));
                                    let selectedSlotCommuteTimeEnd = new Date(moment(selectedSlotCommuteTimeStart).add(minCommutetime, 'm').toDate());
                                    let selectedStartTime = new Date(moment(selectedSlotCommuteTimeEnd));
                                    let selectedEndTime = new Date(slotEnd);


                                    let date = selectedStartTime.getDate();
                                    let month = ((selectedStartTime.getMonth() + 1) < 10 ? '0' : '') + (selectedStartTime.getMonth() + 1);
                                    let year = selectedStartTime.getFullYear();

                                    let startHours = selectedStartTime.getHours();
                                    let startMinutes = ((selectedStartTime.getMinutes()) < 10 ? '0' : '') + (selectedStartTime.getMinutes());
                                    let startSeconds = ((selectedStartTime.getSeconds()) < 10 ? '0' : '') + (selectedStartTime.getSeconds());

                                    let endHours = selectedEndTime.getHours();
                                    let endMinutes = ((selectedEndTime.getMinutes()) < 10 ? '0' : '') + (selectedEndTime.getMinutes());
                                    let endSeconds = ((selectedEndTime.getSeconds()) < 10 ? '0' : '') + (selectedEndTime.getSeconds());

                                    let fullDateStart = year + '-' + month + '-' + date + ' ' + startHours + ':' + startMinutes + ':' + startSeconds;
                                    let fullDateEnd = year + '-' + month + '-' + date + ' ' + endHours + ':' + endMinutes + ':' + endSeconds;

                                    events.push({
                                        allDay: false,
                                        title: 'Commute Time',
                                        category: 'selected-slot-commute',
                                        start: selectedSlotCommuteTimeStart,
                                        end: selectedSlotCommuteTimeEnd,

                                    })

                                    events.push({
                                        allDay: false,
                                        title: 'Installation Time',
                                        category: 'selected-slot-installtion',
                                        start: new Date(selectedStartTime),
                                        end: new Date(selectedEndTime),

                                    })

                                    this.setState({
                                        slots: events,
                                        is_slot_selected: true,
                                        slotSelectedStart: fullDateStart,
                                        slotSelectedEnd: fullDateEnd,
                                        workerId: slotsSortByPriority[i]['worker_id'],
                                        workerName: slotsSortByPriority[i]['worker_name']
                                    });
                                    break;
                                }
                            }
                        }
                        if (slotAvailable) {
                            break;
                        }
                    } else {
                        goToNextWorker = true;
                        break;
                    }
                }

                if (goToNextWorker) {
                    continue;
                }

            }
            if (slotAvailable) {
                break;
            }
        }
    }

    createAppointment() {
        if (this.state.appointmentInProcess) {
            return;
        }
        this.setState({
            appointmentInProcess: true
        });

        let formData = new FormData();
        let params = new URLSearchParams(document.location.search.substring(1));
        let customer_id = params.get("mbr");
        formData.append('worker_id', this.state.workerId);
        formData.append('customer_id', customer_id);
        formData.append('start_time', this.state.slotSelectedStart);
        formData.append('end_time', this.state.slotSelectedEnd);
        formData.append('description', 'test description');

        fetch('https://dispatch1.airebeam.net/api/create_appointment/post', {
            method: 'POST',

            body: formData
        })
                .then(res => res.json())
                .then((jsonResult) => {

                    if (jsonResult.status !== "4") {

                        cookie.save("refreshCalendar", "true", {path: '/'});
                        window.location.reload();

                    } else {
                        this.setState({
                            appointmentCreated: true,
                            appointmentInProcess: false
                        })

                        cookie.remove('tmpMbrAvl', {path: '/'});

                        var interval = setInterval(function () {

                            let count = document.getElementsByClassName("redirectCounter")[0].innerHTML;
                            if (count > 0) {
                                document.getElementsByClassName("redirectCounter")[0].innerHTML = count - 1;
                            }

                            if (count == 0) {
                                window.location = "https://www.airebeam.com";
                                clearInterval(interval);
                            }
                        }, 1000);
                    }
                })
                .catch(err => {
                    this.setState({
                        appointmentInProcess: false
                    });
                });
    }

    confirmAppointment() {
        this.setState({
            confirmAppointment: true
        });
    }

    cancelAppointment() {
        this.setState({
            confirmAppointment: false
        });
    }

    render() {


        let confirmAppointmentDiv = null,
                confirmAppointmentTextDivLeft = null,
                confirmAppointmentTextDivRight = null,
                slotFilledValidation = null,
                loadingMessage = <div className="loader-text top-bottom-padding mob-friendly">                               
                    <p>Please wait while the system is loading the available time slots in your area.</p>
                </div>;

        if (this.state.confirmAppointment) {
            let startDate = new Date(this.state.slotSelectedStart),
                    endDate = new Date(this.state.slotSelectedEnd),
                    locale = "en-us",
                    month = startDate.toLocaleString(locale, {month: "long"}),
                    day = startDate.toLocaleDateString(locale, {weekday: 'long'}),
                    dateFormatted = day + " - " + month + " " + ordinal_suffix_of(startDate.getDate()) + ", " + startDate.getFullYear(),
                    startTime = startDate.toLocaleString('en-US', {hour: 'numeric', minute: 'numeric', hour12: true}),
                    endTime = endDate.toLocaleString('en-US', {hour: 'numeric', minute: 'numeric', hour12: true}),
                    duration = moment(endDate).diff(startDate, 'minutes');

            confirmAppointmentTextDivLeft = <div className="confirm-appt-left">
                <p>Appointment Date: <span>{dateFormatted}</span></p>
                <p>Installer Name: <span>{this.state.workerName}</span></p>
            </div>;

            confirmAppointmentTextDivRight = <div className="confirm-appt-right">
                <p>Expected Start Time: <span>{startTime}</span></p>
                <p>Expected End Time: <span>{endTime}</span></p>
                <p>Expected Duration: <span>{duration} Minutes</span></p>
            </div>;

            confirmAppointmentDiv = <div className="confirm-appt">
                {confirmAppointmentTextDivLeft}
                {confirmAppointmentTextDivRight}
                <div className="clear"></div>
                <div className="confirm-appt-actions">
                    <button className="confirm-appt-btn" onClick={this.createAppointment}>Confirm</button>
                    <button className="cancel-appt-btn" onClick={this.cancelAppointment}>Cancel</button>
                </div>
            </div>;
        }

        if (this.state.refreshCalendar) {
            cookie.remove('refreshCalendar', {path: '/'})
            slotFilledValidation = <div className="slot-filled-validate">
                <span>The slot you selected has been filled, please select another appointment slot</span>
            </div>;
            loadingMessage = <div className="loader-text top-bottom-padding mob-friendly">
                <p>Please wait... </p>
            </div>;
        }

        if (this.state.appointmentCreated) {
            return (
                    <div>
                        <div className="loader-text appt-created">
                            <p>Thank You</p>
                            <p>Your appointment has been confirmed.</p>
                        </div>
                        <div className="confirm-appt">
                            {confirmAppointmentTextDivLeft}
                            {confirmAppointmentTextDivRight}
                            <div className="clear"></div>
                        </div>
                        <div className="loader-text appt-created" style={{marginTop: '20px'}}>
                            <p>The page will be redirected in <span className="redirectCounter">5</span> seconds...</p>
                        </div>
                    
                    </div>
                                )
                    }

            if (this.state.startOfWeek != '') {
                let date = moment(this.state.startOfWeek);
                let dow = date.day();
                console.log(dow);
                moment.updateLocale('en-gb', {
                    week: {
                        dow: dow // make the first day from api start of the week
                    }
                });

                return (
                        <div>
                            <div className="loader-text top-bottom-padding not-mob-friendly">
                                <p>Unable to load, but surely work on PC. You can try</p>
                            </div>
                            {slotFilledValidation}
                            <div className="header-div">
                                <div className="header-color-desc">
                                    <div><div className="color-div booked"></div>Unavailable Slots</div>
                                    <div><div className="color-div available"></div>Available Slots</div>
                                    <div><div className="color-div commute"></div>Expected Commute Time</div>
                                    <div><div className="color-div installation"></div>Expected Installation Time</div>
                                </div>
                                <div className="appointment-action">
                                    <button className="create_app_btn" onClick={this.confirmAppointment} disabled={!this.state.is_slot_selected}>Create Appointment</button>
                                </div>
                                <div className="clear"></div>
                            </div>
                        
                            {confirmAppointmentDiv}
                            <BigCalendar
                                events={this.state.slots}
                                defaultDate={new Date(moment(this.state.startOfWeek))}
                                eventPropGetter={event => ({className: 'category-' + event.category.toLowerCase()})}
                                min={new Date(moment(this.state.startTime))}
                                max={new Date(moment(this.state.endTime))}
                                views={{
                                    week: true
                                }}
                                onSelectSlot = {(slotInfo) => {
                                                this.makeAppoitmentSlot(this.state.workerSlots, slotInfo, this.state.slots);

                                }}
                                onSelecting = {range => false}
                                selectable={true}
                                view={this.state.view}
                                formats={{
                                            dayFormat: (date, culture, localizer) => localizer.format(date, 'ddd M/D', culture)}}
                                timeslots={1}
                                step={30}
                                messages={{'allDay': ''}}
                                selectable = 'ignoreEvents'
                                titleAccessor={(event) => {
                                                    return event.title
                                }}
                                onView={(view) => {
                                                        this.setState({view})
                                }}
                                />
                        
                        </div>
                                                );
                                }
                                document.getElementsByClassName('wp-loader-text')[0].style.display = 'none';
                                return (
                                        <div>
                                            <div className="loader-text top-bottom-padding not-mob-friendly">
                                                <p>Unable to load, but surely work on PC. You can try</p>
                                            </div>
                                            {loadingMessage}
                                        </div>
                                        );
                            }
                        }

                        function toArray(object) {
                            return Object.values(object);
                        }

                        function getTimeRange(slots) {
                            let timeRange = {
                                startTime: slots[0]['start_time'].date,
                                endTime: slots[0]['end_time'].date
                            }
                            for (let i = 1; i < slots.length; i++) {
                                if (timeRange.startTime > slots[i]['start_time'].date) {
                                    timeRange.startTime = slots[i]['start_time'].date;
                                }
                                if (timeRange.endTime > slots[i]['end_time'].date) {
                                    timeRange.endTime = slots[i]['end_time'].date;
                                }
                            }
                            return timeRange;
                        }

                        function getSlots(slotsArray) {
                            let slots = [];
                            let holidays = [];
                            let holidaysWithTitle = [];
                            //get holidays
                            for (let i = 0; i < slotsArray[1].holidays.length; i++) {
                                holidays.push(slotsArray[1].holidays[i].date);
                                holidaysWithTitle[slotsArray[1].holidays[i].date] = slotsArray[1].holidays[i].name;

                            }

                            //get booked slots
                            for (let i = 0; i < slotsArray.length; i++) { //traverse through everyday

                                let start_time = moment(slotsArray[i].start_time.date);
                                let end_time = moment.utc(moment(start_time).add(30, 'm').toDate());

                                //set an all day event if holiday
                                if (holidays.includes(slotsArray[i].date)) {
                                    if (new Date(moment(slotsArray[i].end_time.date)).getMinutes() < 30) {
                                        end_time = new Date(moment(slotsArray[i].end_time.date)).setMinutes(0);
                                        end_time = new Date(moment(end_time)).setSeconds(0);
                                    } else {
                                        end_time = new Date(moment(slotsArray[i].end_time.date)).setMinutes(30);
                                        end_time = new Date(moment(end_time)).setSeconds(0);

                                    }

                                    slots.push({
                                        allDay: false,
                                        title: holidaysWithTitle[slotsArray[i].date],
                                        category: 'holiday-slot',
                                        start: new Date(start_time),
                                        end: new Date(end_time)
                                    })

                                    continue;
                                }

                                //get all slots if not an holiday
                                if (slotsArray[i]['available_slots'].length == 0) {

                                    slots.push({
                                        allDay: false,
                                        title: 'Slot is booked already',
                                        category: 'booked-slots',
                                        start: new Date(start_time),
                                        end: new Date(moment(slotsArray[i].end_time.date))

                                    })
                                    continue;

                                }
                                var max_slot_limit_exceed = true;

                                for (let k = 0; k < slotsArray[i].booked_slots.length; k++) {
                                    if (slotsArray[i].booked_slots[k].worker_booked_slots.length < slotsArray[i].max_appointment_per_day) {
                                        max_slot_limit_exceed = false;
                                    }
                                }

                                if (max_slot_limit_exceed) {
                                    slots.push({
                                        allDay: false,
                                        title: 'Slot is booked already',
                                        category: 'booked-slots',
                                        start: new Date(start_time),
                                        end: new Date(moment(slotsArray[i].end_time.date))

                                    })
                                    continue;

                                }

                                while (end_time < moment(slotsArray[i].end_time.date)) {

                                    for (let j = 0; j < slotsArray[i]['available_slots'].length; j++) {
                                        let available_start_time = moment(slotsArray[i]['available_slots'][j].start_time.date);
                                        let available_end_time = moment(slotsArray[i]['available_slots'][j].end_time.date);

                                        if (moment(start_time) <= available_start_time && moment(end_time) <= available_start_time) {

                                            slots.push({
                                                allDay: false,
                                                title: 'Slot is booked already',
                                                category: 'booked-slots',
                                                start: new Date(start_time),
                                                end: new Date(available_start_time)
                                            })
                                        }

                                        start_time = moment(slotsArray[i]['available_slots'][j].end_time.date);
                                        end_time = moment.utc(moment(start_time).add(30, 'm').toDate());
                                        if (j == slotsArray[i]['available_slots'].length - 1) {
                                            end_time = moment(slotsArray[i].end_time.date);
                                            if (!moment(start_time).isSame(end_time)) {
                                                slots.push({
                                                    allDay: false,
                                                    title: 'Slot is booked already',
                                                    category: 'booked-slots',
                                                    start: new Date(start_time),
                                                    end: new Date(end_time)

                                                })
                                            }
                                        }
                                    }
                                }
                            }

                            return slots;
                        }

                        function getWorkerSlots(slotsArray) {
                            let workerSlots = [];

                            for (let i = 0; i < slotsArray.length; i++) {

                                workerSlots[slotsArray[i].date] = {
                                    'minCommutetime': slotsArray[i].min_commute_time,
                                    'installationTime': slotsArray[i].installation_time,
                                    'startTime': slotsArray[i].start_time,
                                    'endTime': slotsArray[i].end_time,
                                    'maxAppointmentPerDay': slotsArray[i].max_appointment_per_day,
                                    'workerSlots': slotsArray[i].booked_slots
                                }
                            }
                            return workerSlots;
                        }

                        function ordinal_suffix_of(i) {
                            var j = i % 10,
                                    k = i % 100;
                            if (j == 1 && k != 11) {
                                return i + "st";
                            }
                            if (j == 2 && k != 12) {
                                return i + "nd";
                            }
                            if (j == 3 && k != 13) {
                                return i + "rd";
                            }
                            return i + "th";
                        }

                        ReactDOM.render(<MyCalendar />, document.getElementById('root'));
