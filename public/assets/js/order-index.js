jQuery(document).ready(function ($) {

    var add_charges_1 = 25.00; // for only one service - I
    var add_charges_2 = 40.00; // for two service I+TV
    var add_charges_3 = 60.00; // for three service I+TV+DVR
    var add_charges_4 = 75.00; // for four service I+TV+DVR+ Phone

    var plan_price = {
        "0": 0.00,
        "P0105": 39.95 + add_charges_1,
        "P0201": 49.95 + add_charges_1,
        "P0402": 59.95 + add_charges_1,
        "P0603": 69.95 + add_charges_1,
        "P0804": 79.95 + add_charges_1,
        "P1005": 99.95 + add_charges_1,
        "P1508": 149.95 + add_charges_1,
        "P2010": 179.95 + add_charges_1,
        "WC_WiFi": 29.95 + add_charges_1,
    };

    var bundle_price = {
        "0": 0.00,
        "B1_P0603": 92 + add_charges_2,
        "B1_P0804": 102 + add_charges_2,
        "B1_P1005": 121 + add_charges_2,
        "B1_P1508": 168 + add_charges_2,
        "B1_P2010": 197 + add_charges_2,

        "B2_P0603": 99 + add_charges_3,
        "B2_P0804": 108 + add_charges_3,
        "B2_P1005": 127 + add_charges_3,
        "B2_P1508": 175 + add_charges_3,
        "B2_P2010": 203 + add_charges_3,

        "B3_P0603": 111 + add_charges_3,
        "B3_P0804": 121 + add_charges_3,
        "B3_P1005": 140 + add_charges_3,
        "B3_P1508": 187 + add_charges_3,
        "B3_P2010": 216 + add_charges_3,

        "B4_P0603": 118 + add_charges_4,
        "B4_P0804": 127 + add_charges_4,
        "B4_P1005": 146 + add_charges_4,
        "B4_P1508": 194 + add_charges_4,
        "B4_P2010": 222 + add_charges_4,
    }

    var stream_vision_add_tv_price = 4.95;
    var dvr_add_tv_price = 3.95;
    var dvr_add_storage_price = 6.95;

    var phone_packages_mapping = {
        "0": 0.00,
        //E-Phone Canada
        "1": 3.00,
        //E-Phone International
        "2": 0.00
    };

    var six_mbps_bundles = {
        "B1_P0603": "P0603 6/3 MBPS - $92 (I+TV)",
        "B2_P0603": "P0603 6/3 MBPS - $99 (I+TV+DVR)",
        "B3_P0603": "P0603 6/3 MBPS - $111 (I+TV+Phone)",
        "B4_P0603": "P0603 6/3 MBPS - $118 All Four(4)",
    };

    var eight_mbps_bundles = {
        "B1_P0804": "P0804 8/4 MBPS - $102 (I+TV)",
        "B2_P0804": "P0804 8/4 MBPS - $108 (I+TV+DVR)",
        "B3_P0804": "P0804 8/4 MBPS - $121 (I+TV+Phone)",
        "B4_P0804": "P0804 8/4 MBPS - $127 All Four(4)",
    };

    var ten_mbps_bundles = {
        "B1_P1005": "P1005 10/05 MBPS - $121 (I+TV)",
        "B2_P1005": "P1005 10/05 MBPS - $127 (I+TV+DVR)",
        "B3_P1005": "P1005 10/05 MBPS - $140 (I+TV+Phone)",
        "B4_P1005": "P1005 10/05 MBPS - $146 All Four(4)",
    };

    var sixteen_mbps_bundles = {
        "B1_P1508": "P1508 15/08 MBPS - $168 (I+TV)",
        "B2_P1508": "P1508 15/08 MBPS - $175 (I+TV+DVR)",
        "B3_P1508": "P1508 15/08 MBPS - $187 (I+TV+Phone)",
        "B4_P1508": "P1508 15/08 MBPS - $194 All Four(4)",
    };

    var twenty_mbps_bundles = {
        "B1_P2010": "P2010 20/10 MBPS - $197 (I+TV)",
        "B2_P2010": "P2010 20/10 MBPS - $203 (I+TV+DVR)",
        "B3_P2010": "P2010 20/10 MBPS - $216 (I+TV+Phone)",
        "B4_P2010": "P2010 20/10 MBPS - $222 All Four(4)",
    };

    $('.ab-plan select').change(function () {
        $this = $(this);
        $bundle_select = $('.ab-bundle select');
        $sv_add_tv_row = $('.ab-sv-add-tv');
        $dvr_add_tv_row = $('.ab-dvr-add-tv');
        $dvr_add_storage_row = $('.ab-dvr-add-storage');
        $phone_addon_row = $('.ab-phone-addon');
        $sv_add_tv_row.hide();
        $dvr_add_tv_row.hide();
        $dvr_add_storage_row.hide();
        $phone_addon_row.hide();

        var bundles = {};
        if ($this.val() == "P0603") {
            bundles = six_mbps_bundles;
        } else if ($this.val() == "P0804") {
            bundles = eight_mbps_bundles;
        } else if ($this.val() == "P1005") {
            bundles = ten_mbps_bundles;
        } else if ($this.val() == "P1508") {
            bundles = sixteen_mbps_bundles;
        } else if ($this.val() == "P2010") {
            bundles = twenty_mbps_bundles;
        }

        var options = '<option value="0"> Please select... </option>';
        $.each(bundles, function (key, value) {
            options += '<option value="' + key + '">' + value + '</option>';
        });
        $bundle_select.html(options);

    });

    $('.ab-plan select').change(function () {
        var cal_price = 0;
        $this = $(this);
        cal_price = plan_price[$this.val()];
        cal_price = cal_price.toFixed(2);
        $('#show_calculation').html(cal_price);
    });

    $('.ab-bundle select').change(function () {
        $('.ab-sv-add-tv select').val(0);
        $('.ab-dvr-add-tv select').val(0);
        $('.ab-dvr-add-storage select').val(0);
        $('.ab-phone-addon select').val(0);

        var cal_price = 0;
        $this = $(this);
        if ($this.val() == "0") {
            cal_price = plan_price[ $('.ab-plan select').val() ]
        } else {
            cal_price = bundle_price[$this.val()];
        }
        cal_price = cal_price.toFixed(2);
        $('#show_calculation').html(cal_price);
    });

    $('.ab-sv-add-tv select').change(function () {
        var cal_price = 0;
        $this = $(this);
        $bundle_price = bundle_price[ $('.ab-bundle select').val() ];

        $dvr_add_tv_price = (parseInt($('.ab-dvr-add-tv select').val()) * dvr_add_tv_price);
        $dvr_add_storage_price = (parseInt($('.ab-dvr-add-storage select').val()) * dvr_add_storage_price);
        $phone_price = phone_packages_mapping[ $('.ab-phone-addon select').val() ];

        cal_price = (parseInt($this.val()) * stream_vision_add_tv_price) + $phone_price + $dvr_add_tv_price + $dvr_add_storage_price + $bundle_price;
        cal_price = cal_price.toFixed(2);
        $('#show_calculation').html(cal_price);
    });

    $('.ab-dvr-add-tv select').change(function () {
        var cal_price = 0;
        $this = $(this);
        $bundle_price = bundle_price[ $('.ab-bundle select').val() ];

        $sv_add_tv_price = (parseInt($('.ab-sv-add-tv select').val()) * stream_vision_add_tv_price);
        $dvr_add_storage_price = (parseInt($('.ab-dvr-add-storage select').val()) * dvr_add_storage_price);
        $phone_price = phone_packages_mapping[ $('.ab-phone-addon select').val() ];

        cal_price = (parseInt($this.val()) * dvr_add_tv_price) + $phone_price + $dvr_add_storage_price + $sv_add_tv_price + $bundle_price;
        cal_price = cal_price.toFixed(2);
        $('#show_calculation').html(cal_price);
    });

    $('.ab-dvr-add-storage select').change(function () {
        var cal_price = 0;
        $this = $(this);
        $bundle_price = bundle_price[ $('.ab-bundle select').val() ];
        $sv_add_tv_price = (parseInt($('.ab-sv-add-tv select').val()) * stream_vision_add_tv_price);
        $dvr_add_tv_price = (parseInt($('.ab-dvr-add-tv select').val()) * dvr_add_tv_price);
        $phone_price = phone_packages_mapping[ $('.ab-phone-addon select').val() ];

        cal_price = (parseInt($this.val()) * dvr_add_storage_price) + $phone_price + $dvr_add_tv_price + $sv_add_tv_price + $bundle_price;
        cal_price = cal_price.toFixed(2);
        $('#show_calculation').html(cal_price);
    });

    $('.ab-phone-addon select').change(function () {

        var cal_price = 0;
        $this = $(this);
        $bundle_price = bundle_price[ $('.ab-bundle select').val() ];
        $sv_add_tv_price = (parseInt($('.ab-sv-add-tv select').val()) * stream_vision_add_tv_price);
        $dvr_add_tv_price = (parseInt($('.ab-dvr-add-tv select').val()) * dvr_add_tv_price);
        $dvr_add_storage_price = (parseInt($('.ab-dvr-add-storage select').val()) * dvr_add_storage_price);
        $phone_price = phone_packages_mapping[ $this.val() ];

        cal_price = $phone_price + $dvr_add_storage_price + $dvr_add_tv_price + $sv_add_tv_price + $bundle_price;
        cal_price = cal_price.toFixed(2);
        $('#show_calculation').html(cal_price);

    });



});